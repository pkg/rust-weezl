Source: rust-weezl
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Alexander Kjäll <alexander.kjall@gmail.com>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/weezl]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/weezl
Rules-Requires-Root: no

Package: librust-weezl-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends}
Suggests:
 librust-weezl+async-dev (= ${binary:Version}),
 librust-weezl+futures-dev (= ${binary:Version})
Provides:
 librust-weezl+alloc-dev (= ${binary:Version}),
 librust-weezl+default-dev (= ${binary:Version}),
 librust-weezl+std-dev (= ${binary:Version}),
 librust-weezl-0-dev (= ${binary:Version}),
 librust-weezl-0+alloc-dev (= ${binary:Version}),
 librust-weezl-0+default-dev (= ${binary:Version}),
 librust-weezl-0+std-dev (= ${binary:Version}),
 librust-weezl-0.1-dev (= ${binary:Version}),
 librust-weezl-0.1+alloc-dev (= ${binary:Version}),
 librust-weezl-0.1+default-dev (= ${binary:Version}),
 librust-weezl-0.1+std-dev (= ${binary:Version}),
 librust-weezl-0.1.5-dev (= ${binary:Version}),
 librust-weezl-0.1.5+alloc-dev (= ${binary:Version}),
 librust-weezl-0.1.5+default-dev (= ${binary:Version}),
 librust-weezl-0.1.5+std-dev (= ${binary:Version})
Description: Fast LZW compression and decompression - Rust source code
 This package contains the source for the Rust weezl crate, packaged by debcargo
 for use with cargo and dh-cargo.

Package: librust-weezl+async-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-weezl-dev (= ${binary:Version}),
 librust-weezl+futures-dev (= ${binary:Version}),
 librust-weezl+std-dev (= ${binary:Version})
Provides:
 librust-weezl-0+async-dev (= ${binary:Version}),
 librust-weezl-0.1+async-dev (= ${binary:Version}),
 librust-weezl-0.1.5+async-dev (= ${binary:Version})
Description: Fast LZW compression and decompression - feature "async"
 This metapackage enables feature "async" for the Rust weezl crate, by pulling
 in any additional dependencies needed by that feature.

Package: librust-weezl+futures-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-weezl-dev (= ${binary:Version}),
 librust-futures-0.3+std-dev (>= 0.3.12-~~)
Provides:
 librust-weezl-0+futures-dev (= ${binary:Version}),
 librust-weezl-0.1+futures-dev (= ${binary:Version}),
 librust-weezl-0.1.5+futures-dev (= ${binary:Version})
Description: Fast LZW compression and decompression - feature "futures"
 This metapackage enables feature "futures" for the Rust weezl crate, by pulling
 in any additional dependencies needed by that feature.
